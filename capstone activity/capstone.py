class Person:
    def __init__(self, first_name, last_name, email, department):
        self._first_name = first_name
        self._last_name = last_name
        self._email = email
        self._department = department
        self._requests = []

    def getFullName(self):
        return f"{self._first_name} {self._last_name}"

    def addRequest(self):
        return "Request has been added"

    def checkRequest(self):
        return "Requests: " + ", ".join([r.name for r in self._requests])

    def addUser(self):
        pass


class Employee(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name, email, department)

    def checkRequest(self):
        return "Employee Request: " + super().checkRequest()

    def addUser(self):
        pass

    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"


class TeamLead(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name, email, department)
        self._members = []

    def checkRequest(self):
        return "TeamLead Request: " + super().checkRequest()

    def addUser(self):
        pass

    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"

    def addMember(self, employee):
        self._members.append(employee)


class Admin(Person):
    def __init__(self, first_name, last_name, email, department):
        super().__init__(first_name, last_name, email, department)

    def checkRequest(self):
        return "Admin Request: " + super().checkRequest()

    def addUser(self):
        return "New user added"

    def login(self):
        return f"{self._email} has logged in"

    def logout(self):
        return f"{self._email} has logged out"


class Request:
    def __init__(self, name, requester, date_requested):
        self.name = name
        self.requester = requester
        self.date_requested = date_requested
        self.status = "Open"

    def updateRequest(self, status):
        self.status = status

    def closeRequest(self):
        self.status = "Closed"

    def cancelRequest(self):
        self.status = "Cancelled"


# Test Cases
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")


#Capstone Specifications
assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"
